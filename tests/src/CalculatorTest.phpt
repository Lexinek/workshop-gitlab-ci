<?php

namespace Tests;

use Nette;
use Tester;
use Tester\Assert;
use Workshop\Calculator;

$container = require __DIR__ . '/../bootstrap.php';


class CalculatorTest extends Tester\TestCase
{
	private $container;


	public function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
	}


	public function testAdd()
	{
		$c = new Calculator();

		Assert::same(4, $c->add(2,2));
	}

}


(new CalculatorTest($container))->run();
